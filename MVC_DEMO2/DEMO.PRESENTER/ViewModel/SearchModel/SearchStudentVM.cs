﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DEMO.PRESENTER.SearchModel
{
    public class SearchStudentVM
    {
        public SearchStudentVM()
        {
        }
        public string StudentCode { get; set; }
        public string ClassId { get; set; }
        public string LastName { get; set; }
        public string FirstMidName { get; set; }

        [DataType(DataType.Date)]
        public DateTime FromDate { get; set; }

        [DataType(DataType.Date)]
        public DateTime ToDate { get; set; }

        public int PageSize { get; set; }
        public int PageIndex { get; set; }
    }
}