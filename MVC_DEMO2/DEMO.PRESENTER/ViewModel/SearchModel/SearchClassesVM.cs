﻿namespace DEMO.PRESENTER.ViewModel.SearchModel
{
    public class SearchClassesVM
    {
        public SearchClassesVM()
        {

        }
        public string ClassCode { get; set; }
        public string ClassName { get; set; }

        public int pageSize { get; set; }
        public int pageIndex { get; set; }
    }
}