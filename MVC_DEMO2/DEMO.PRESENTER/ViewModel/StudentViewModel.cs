﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DEMO.PRESENTER.ViewModel
{
    public class StudentViewModel
    {
        public StudentViewModel()
        {
        }
        public int ID { get; set; }
        [Required]
        [Display(Name = "Student Code")]
        public string StudentCode { get; set; }
        [Required]
        [Display(Name = "Class Name")]
        public string ClassId { get; set; }
        [Required]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }
        [Required]
        [Display(Name = "First Name")]
        public string FirstMidName { get; set; }
        [Required]
        [DataType(DataType.Date)]
        public DateTime EnrollmentDate { get; set; }

    }
}