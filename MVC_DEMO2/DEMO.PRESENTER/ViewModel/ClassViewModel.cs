﻿using System.ComponentModel.DataAnnotations;

namespace DEMO.PRESENTER.ViewModel
{
    public class ClassViewModel
    {
        public int ID { get; set; }
        [Required]
        [Display(Name = "Class Code")]
        public string ClassCode { get; set; }
        [Required]
        [Display(Name = "Class Name")]
        public string ClassName { get; set; }

    }
}