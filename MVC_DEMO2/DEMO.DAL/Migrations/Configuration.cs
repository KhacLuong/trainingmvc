﻿namespace DEMO.DAL.Migrations
{
    using DEMO.DAL.Models;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<DEMO.DAL.Models.SchoolContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(DEMO.DAL.Models.SchoolContext context)
        {  
            //delete all data
            var stringsql = @"
                            TRUNCATE TABLE [MVC_DEMO2].[dbo].[Class]
                            TRUNCATE TABLE [MVC_DEMO2].[dbo].[Student]  
                            TRUNCATE TABLE [MVC_DEMO2].[dbo].[UserMaster]
                            ";
            context.Database.ExecuteSqlCommand(stringsql);

            //create new data
            var userMaster = new UserMaster
            {
                UserName = "KhacLuong",
                Password = "P@ssword00",
                UserEmail = "khacluong@gmail.com",
            };
            context.UserMaster.Add(userMaster);


            var classes = new List<Class>
            {
                new Class
                {
                 ClassCode = Guid.NewGuid().ToString(),
                 ClassName = "Class000001"
                },
                new Class
                {
                 ClassCode = Guid.NewGuid().ToString(),
                 ClassName = "Class000002"
                },
                new Class
                {
                 ClassCode = Guid.NewGuid().ToString(),
                 ClassName = "Class000003"
                },
                new Class{
                 ClassCode = Guid.NewGuid().ToString(),
                 ClassName = "Class000004"
                },
                  new Class{
                 ClassCode = Guid.NewGuid().ToString(),
                 ClassName = "Class000005"
                },
                    new Class{
                 ClassCode = Guid.NewGuid().ToString(),
                 ClassName = "Class000006"
                },
                      new Class{
                 ClassCode = Guid.NewGuid().ToString(),
                 ClassName = "Class000007"
                },
                        new Class{
                 ClassCode = Guid.NewGuid().ToString(),
                 ClassName = "Class000008"
                },
                          new Class{
                 ClassCode = Guid.NewGuid().ToString(),
                 ClassName = "Class000009"
                },
            };
            classes.ForEach(c => context.Classes.AddOrUpdate(x => x.ClassCode, c));
            context.SaveChanges();

            var students = new List<Student>
            {
                new Student {
                    ClassId = classes.SingleOrDefault(c => c.ClassName == "Class000001").ID,
                    StudentCode = Guid.NewGuid().ToString(),
                    FirstMidName = "Carson",
                    LastName = "Alexander",
                    EnrollmentDate = DateTime.Parse("2010-09-01")
                },
                new Student {
                    ClassId = classes.SingleOrDefault(c => c.ClassName == "Class000001").ID,
                    StudentCode =Guid.NewGuid().ToString(),
                    FirstMidName = "Meredith",
                    LastName = "Alonso",
                    EnrollmentDate = DateTime.Parse("2012-09-01")
                },
                new Student {
                    ClassId = classes.SingleOrDefault(c => c.ClassName == "Class000002").ID,
                    StudentCode = Guid.NewGuid().ToString(),
                    FirstMidName = "Arturo",
                    LastName = "Anand",
                    EnrollmentDate = DateTime.Parse("2013-09-01")
                },
                new Student {
                    ClassId = classes.SingleOrDefault(c => c.ClassName == "Class000002").ID,
                    StudentCode = Guid.NewGuid().ToString(),
                    FirstMidName = "Gytis",
                    LastName = "Barzdukas",
                    EnrollmentDate = DateTime.Parse("2012-09-01") },
                new Student {
                    ClassId = classes.SingleOrDefault(c => c.ClassName == "Class000003").ID,
                    StudentCode = Guid.NewGuid().ToString(),
                    FirstMidName = "Yan",
                    LastName = "Li",
                    EnrollmentDate = DateTime.Parse("2012-09-01")
                },
                new Student {
                    ClassId = classes.SingleOrDefault(c => c.ClassName == "Class000003").ID,
                    StudentCode = Guid.NewGuid().ToString(),
                    FirstMidName = "Peggy",
                    LastName = "Justice",
                    EnrollmentDate = DateTime.Parse("2011-09-01")
                },
                new Student {
                    ClassId = classes.SingleOrDefault(c => c.ClassName == "Class000004").ID,
                    StudentCode = Guid.NewGuid().ToString(),
                    FirstMidName = "Laura",
                    LastName = "Norman",
                    EnrollmentDate = DateTime.Parse("2013-09-01")
                },
                new Student {
                    ClassId = classes.SingleOrDefault(c => c.ClassName == "Class000004").ID,
                    StudentCode = Guid.NewGuid().ToString(),
                    FirstMidName = "Nino",
                    LastName = "Olivetto",
                    EnrollmentDate = DateTime.Parse("2005-08-11")
                },
                  new Student {
                    ClassId = classes.SingleOrDefault(c => c.ClassName == "Class000004").ID,
                    StudentCode = Guid.NewGuid().ToString(),
                    FirstMidName = "Nino",
                    LastName = "Olivetto",
                    EnrollmentDate = DateTime.Parse("2005-08-11")
                },
                    new Student {
                    ClassId = classes.SingleOrDefault(c => c.ClassName == "Class000004").ID,
                    StudentCode = Guid.NewGuid().ToString(),
                    FirstMidName = "Nino",
                    LastName = "Olivetto",
                    EnrollmentDate = DateTime.Parse("2005-08-11")
                },
            };
            students.ForEach(s => context.Students.AddOrUpdate(p => p.LastName, s));
            context.SaveChanges();

        }
    }
}
