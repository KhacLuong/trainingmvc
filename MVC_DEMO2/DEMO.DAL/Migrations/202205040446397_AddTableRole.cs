﻿namespace DEMO.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTableRole : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.RoleMaster",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RoleName = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.UserRolesMapping",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        UserId = c.Int(nullable: false),
                        RoleID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            AddColumn("dbo.UserMaster", "RoleId", c => c.String());
            DropColumn("dbo.UserMaster", "UserRole");
        }
        
        public override void Down()
        {
            AddColumn("dbo.UserMaster", "UserRole", c => c.String());
            DropColumn("dbo.UserMaster", "RoleId");
            DropTable("dbo.UserRolesMapping");
            DropTable("dbo.RoleMaster");
        }
    }
}
