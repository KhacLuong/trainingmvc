﻿namespace DEMO.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class createforeignkey : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.Student", "ClassId");
            AddForeignKey("dbo.Student", "ClassId", "dbo.Class", "ID", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Student", "ClassId", "dbo.Class");
            DropIndex("dbo.Student", new[] { "ClassId" });
        }
    }
}
