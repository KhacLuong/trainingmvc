﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DEMO.DAL.Models
{
    public class UserRolesMapping
    {
        public int ID { get; set; }
        public int UserId { get; set; }
        public int RoleID { get; set; }
    }
}