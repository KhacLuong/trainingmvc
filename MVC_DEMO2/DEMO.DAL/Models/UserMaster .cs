﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DEMO.DAL.Models
{
    public class UserMaster
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string RoleId { get; set; }
        public string UserEmail { get; set; }
    }
}