﻿namespace DEMO.DAL.Models
{
    public class Class
    {
        public int ID { get; set; }
        public string ClassCode { get; set; }
        public string ClassName { get; set; }
    }
}