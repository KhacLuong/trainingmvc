﻿
namespace DEMO.DAL.Models
{
    public class RoleMaster
    {
        public int Id { get; set; }
        public string RoleName { get; set; }
    }
}