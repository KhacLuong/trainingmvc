﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace DEMO.DAL.Models
{
    public class SchoolContext : DbContext
    {
        public SchoolContext() : base("mvc_demo2")
        {
        }

        public DbSet<Student> Students { get; set; }
        public DbSet<Class> Classes { get; set; }
        public DbSet<UserMaster> UserMaster { get; set; }
        public DbSet<RoleMaster> RoleMaster { get; set; }
        public DbSet<UserRolesMapping> UserRolesMapping { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}