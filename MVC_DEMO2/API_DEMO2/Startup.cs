﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(API_DEMO2.Startup))]

namespace API_DEMO2
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            //ConfigureAuth(app);
            ConfigureJWTAuth(app);
        }
    }
}
