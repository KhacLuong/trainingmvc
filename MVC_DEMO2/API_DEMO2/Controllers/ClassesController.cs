﻿using API_DEMO2.FilterAttribute;
using AutoMapper;
using DEMO.DAL.Models;
using DEMO.PRESENTER.ViewModel;
using DEMO.PRESENTER.ViewModel.SearchModel;
using DEMO.REPOSITORY.Repository;
using DEMO.REPOSITORY.Repository.ClassRepo;
using DEMO.REPOSITORY.Repository.Generic;
using DEMO.REPOSITORY.Repository.StudentRepo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace API_DEMO2.Controllers
{
    [JWTAuthentication(Rolename = "Admin")]
    public class ClassesController : ApiController
    {
        private readonly IGenericRepository _genericRepository;
        private readonly IClassRepository _classRepository;
        private readonly IStudentRepository _studentRepository;
        private IUnitOfWork _unitOfWork;
        public ClassesController(
            SchoolContext context,
            IGenericRepository genericRepository,
            IClassRepository classRepository,
            IStudentRepository studentRepository
            )
        {
            _genericRepository = new GenericRepository(context);
            _classRepository = new ClassRepository(context);
            _studentRepository = new StudentRepository(context);
            _unitOfWork = _genericRepository.UnitOfWork;
        }

        [HttpGet]
        public List<ClassViewModel> GetClasses()
        {
            Mapper.CreateMap<Class, ClassViewModel>();
            var listClass = _classRepository.GetAll<Class>();
            return listClass.Select(x => Mapper.Map<Class, ClassViewModel>(x)).ToList();
        }

        [HttpGet]
        public ClassViewModel GetClassById(int id)
        {
            Mapper.CreateMap<Class, ClassViewModel>();
            var currentClass = _classRepository.GetByID<Class>(id);
            if (currentClass != null)
            {
                var classVM = Mapper.Map<Class, ClassViewModel>(currentClass);
                return classVM;
            }
            return null;
        }

        public IEnumerable<ClassViewModel> GetClassesByCondition(SearchClassesVM searchModel)
        {
            var query = _classRepository.Query<Class>();
            if (searchModel.ClassCode != null)
            {
                query = query.Where(x => x.ClassCode == searchModel.ClassCode);
            }
            if (searchModel.ClassName != null)
            {
                query = query.Where(x => x.ClassName == searchModel.ClassName);
            }

            Mapper.CreateMap<Class, ClassViewModel>();
            return query.ToList().Select(x => Mapper.Map<Class, ClassViewModel>(x)).ToList();
        }

        public bool CreateClass(ClassViewModel classVM)
        {
            try
            {
                Mapper.CreateMap<ClassViewModel, Class>();
                var currentClass = Mapper.Map<ClassViewModel, Class>(classVM);
                var result = _classRepository.Insert(currentClass);
                _unitOfWork.Complete();
                return true;
            }
            catch (Exception ex)
            {
                _unitOfWork.Rollback();
                return false;
            }
        }

        public bool UpdateClass(ClassViewModel classVM)
        {
            try
            {
                Mapper.CreateMap<ClassViewModel, Class>();
                var currentClass = Mapper.Map<ClassViewModel, Class>(classVM);
                _classRepository.Update(currentClass);
                _unitOfWork.Complete();
                return true;
            }
            catch
            {
                _unitOfWork.Rollback();
                return false;
            }

        }

        [HttpGet]
        public bool DeleteClass(int id)
        {
            try
            {
                _classRepository.Delete<Class>(id);
                var listStudentDelete = _studentRepository.Query<Student>()
                .Where(student => student.ClassId == id).ToList();
                foreach (var student in listStudentDelete)
                {
                    _studentRepository.Delete<Student>(student.ID);
                }
                _unitOfWork.Complete();
                return true;
            }
            catch
            {
                _unitOfWork.Rollback();
                return false;
            }
        }
        [HttpPost]
        public bool DeleteSelected(List<int> ids)
        {
            try
            {
                foreach (int id in ids)
                {
                    _classRepository.Delete<Class>(id);
                    var listStudentDelete = _studentRepository.Query<Student>()
                    .Where(student => student.ClassId == id).ToList();
                    foreach (var student in listStudentDelete)
                    {
                        _studentRepository.Delete<Student>(student.ID);
                    }
                }
                _unitOfWork.Complete();
                return true;
            }
            catch (Exception ex)
            {
                _unitOfWork.Rollback();
                return false;
            }
        }
    }
}