﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using API_DEMO2.FilterAttribute;
using AutoMapper;
using DEMO.DAL.Models;
using DEMO.PRESENTER.SearchModel;
using DEMO.PRESENTER.ViewModel;
using DEMO.REPOSITORY.Repository;
using DEMO.REPOSITORY.Repository.ClassRepo;
using DEMO.REPOSITORY.Repository.Generic;
using DEMO.REPOSITORY.Repository.StudentRepo;
using PagedList;

namespace API_DEMO2.Controllers
{
    [JWTAuthentication(Rolename = "Admin")]
    public class StudentsController : ApiController
    {
        private readonly IGenericRepository _genericRepository;
        private readonly IStudentRepository _studentRepository;
        private readonly IClassRepository _classRepository;
        private IUnitOfWork _unitOfWork;
        public StudentsController(
              SchoolContext context,
            IGenericRepository genericRepository,
            IClassRepository classRepository,
            IStudentRepository studentRepository
            )
        {
            _genericRepository = new GenericRepository(context);
            _classRepository = new ClassRepository(context);
            _studentRepository = new StudentRepository(context);
            _unitOfWork = _genericRepository.UnitOfWork;
        }
        // GET: api/Students
        [HttpGet]
        public IEnumerable<StudentViewModel> GetStudents()
        {
            Mapper.CreateMap<Student, StudentViewModel>();
            List<Student> listStudent = _studentRepository.GetAll<Student>() as List<Student>;
            List<StudentViewModel> listStudentVM = new List<StudentViewModel>();

            foreach (Student student in listStudent)
            {
                var studentVM = Mapper.Map<Student, StudentViewModel>(student);
                listStudentVM.Add(studentVM);
            }
            return listStudentVM;
        }
        [HttpGet]
        public StudentViewModel GetStudentsById(int id)
        {
            Student student = _studentRepository.GetByID<Student>(id);
            if (student == null)
            {
                return null;
            }
            Mapper.CreateMap<Student, StudentViewModel>();
            var studentVM = Mapper.Map<Student, StudentViewModel>(student);
            return studentVM;
        }

        [HttpPost]
        public IEnumerable<StudentViewModel> GetStudentByCondition(SearchStudentVM searchModel)
        {
            var query = _studentRepository.Query<Student>();

            if (searchModel.LastName != null)
            {
                query = query.Where(x => x.LastName == searchModel.LastName);
            }
            if (searchModel.FirstMidName != null)
            {
                query = query.Where(x => x.FirstMidName == searchModel.FirstMidName);
            }
            if (searchModel.StudentCode != null)
            {
                query = query.Where(x => x.StudentCode == searchModel.StudentCode);
            }
            if (searchModel.FromDate.Ticks > 0)
            {
                query = query.Where(x => x.EnrollmentDate > searchModel.FromDate);
            }
            if (searchModel.ToDate.Ticks > 0)
            {
                query = query.Where(x => x.EnrollmentDate < searchModel.ToDate);
            }

            Mapper.CreateMap<Class, ClassViewModel>();
            var listStudentSearch = query.ToList();
            return listStudentSearch.Select(x => Mapper.Map<Student, StudentViewModel>(x));
        }

        [HttpGet]
        public bool DeleteStudent(int id)
        {
            if (id > 0)
            {
                _studentRepository.Delete<Student>(id);
                _unitOfWork.Complete();
                return true;
            }
            return false;
        }

        [HttpPost]
        public StudentViewModel CreateStudent(StudentViewModel studentvm)
        {
            Mapper.CreateMap<StudentViewModel, Student>();
            var student = Mapper.Map<StudentViewModel, Student>(studentvm);
            _studentRepository.Insert<Student>(student);
            _unitOfWork.Complete();
            return studentvm;
        }

        [HttpPost]
        public StudentViewModel UpdateStudent(StudentViewModel studentvm)
        {
            Mapper.CreateMap<StudentViewModel, Student>();
            var student = Mapper.Map<StudentViewModel, Student>(studentvm);
            _studentRepository.Update<Student>(student);
            _unitOfWork.Complete();
            return studentvm;
        }

    }
}