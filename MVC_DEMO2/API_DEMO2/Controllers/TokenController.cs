﻿using DEMO.DAL.Models;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Text;
using System.Web.Http;

namespace API_DEMO2.Controllers
{
    [RoutePrefix("api/Token")]
    public class TokenController : ApiController
    {
        private SchoolContext _schoolContext;
        public TokenController()
        {
            _schoolContext = new SchoolContext();
        }
        [HttpPost, Route("GetToken")]
        public object GetToken(ModelLogin model)
        {
            //get current user and roles
            var findUserMaster = _schoolContext.UserMaster.Where(user => user.UserEmail == model.UserEmail && user.Password == model.Password).FirstOrDefault();
            if(findUserMaster == null)
            {
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Unauthorized, new ErrorRespone { ErrorCode = 401, ErrorMessage = "Unvalid UserEmail or Password" });
                return response;
            }
            var findRole = _schoolContext.RoleMaster.Where(role => role.Id.ToString() == findUserMaster.RoleId).FirstOrDefault();
            
            //Create a List of Claims, Keep claims name short    
            var permClaims = new List<Claim>();
            permClaims.Add(new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()));
            permClaims.Add(new Claim("rolename", findRole.RoleName));
            permClaims.Add(new Claim("username", findUserMaster.UserName));
            permClaims.Add(new Claim("useremail", findUserMaster.UserEmail));

            var secretKey = ConfigurationManager.AppSettings["SecretKey"];
            var issuer = ConfigurationManager.AppSettings["Issuer"];
            var audienceId = ConfigurationManager.AppSettings["AudienceId"];

            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(secretKey));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            //Create Security Token object by giving required parameters    
            var token = new JwtSecurityToken(
                            issuer, //Issure    
                            audienceId,  //Audience    
                            permClaims,
                            notBefore: DateTime.UtcNow,
                            expires: DateTime.Now.AddDays(1),
                            signingCredentials: credentials);
            var jwt_token = new JwtSecurityTokenHandler().WriteToken(token);
            return new { data = jwt_token };
        }

        public class ModelLogin
        {
            public string UserEmail { get; set; }

            public string Password { get; set; }
        }
    }
}
