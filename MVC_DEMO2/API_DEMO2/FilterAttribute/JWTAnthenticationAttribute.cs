﻿using DEMO.DAL.Models;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json.Linq;
using System;
using System.Configuration;
using System.IdentityModel.Tokens.Jwt;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace API_DEMO2.FilterAttribute
{
    public class JWTAuthenticationAttribute : AuthorizationFilterAttribute
    {
        private string _rolename;
        public string Rolename
        {
            get { return _rolename; }
            set { _rolename = value; }
        }
        public JWTAuthenticationAttribute()
        {

        }
        public JWTAuthenticationAttribute(string rolename)
        {
            _rolename = rolename;
        }
        public override void OnAuthorization(HttpActionContext actionContext)
        {
            try
            {
                if (actionContext.Request.Headers.Authorization != null)
                {
                    var token = actionContext.Request.Headers
                        .Authorization.Parameter;

                    var handler = new JwtSecurityTokenHandler();
                    var jwtSecurityToken = handler.ReadJwtToken(token);

                    var tokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        ValidateIssuerSigningKey = true,
                        ValidIssuer = ConfigurationManager.AppSettings["Issuer"],
                        ValidAudience = ConfigurationManager.AppSettings["AudienceId"],
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(ConfigurationManager.AppSettings["SecretKey"]))
                    };

                    //verify token for access
                    SecurityToken validateToken = null;
                    ClaimsPrincipal claimsPrincipal = handler.ValidateToken(token, tokenValidationParameters, out validateToken);
                    if (validateToken != null)
                    {
                        ClaimsIdentity identity = null;
                        identity = claimsPrincipal.Identity as ClaimsIdentity;
                        Claim usernameClaim = identity.FindFirst("username");
                        string username = usernameClaim.Value;
                        Claim rolenameClaim = identity.FindFirst("rolename");
                        string rolename = rolenameClaim.Value;
                        if (_rolename != null)
                        {
                            if (rolename == _rolename)
                            {
                                Thread.CurrentPrincipal = new GenericPrincipal(new GenericIdentity(username, rolename), null);
                            }
                            else
                            {
                                actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized, new { ErrorCode = 401, ErrorMessage = "Not have permission to access this api" });
                            }
                        }
                    }
                    else
                    {
                        actionContext.Response = actionContext.Request
                       .CreateResponse(HttpStatusCode.Unauthorized, new { ErrorCode = 401, ErrorMessage = "Unauthorized" });
                    }

                }
                else
                {
                    actionContext.Response = actionContext.Request
                          .CreateResponse(HttpStatusCode.Unauthorized, new { ErrorCode = 401, ErrorMessage = "Unauthorized" });
                }
                base.OnAuthorization(actionContext);
            }
            catch (Exception ex)
            {
                actionContext.Response = actionContext.Request
                        .CreateResponse(HttpStatusCode.Unauthorized, new { ErrorCode = 401, ErrorMessage = ex.Message });
            }
        }
    }
}