﻿
using DEMO.DAL.Models;
using System;
using System.Data.Entity;
using System.Linq;

namespace DEMO.REPOSITORY.Repository
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly SchoolContext _context;
        public UnitOfWork(SchoolContext context)
        {
            _context = context;

        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
        }

        public int Complete()
        {
            return _context.SaveChanges();
        }

        public void Rollback()
        {
            _context
                .ChangeTracker
                .Entries()
                .ToList()
                .ForEach(x => x.Reload());
        }
    }
}