﻿using DEMO.DAL.Models;
using DEMO.REPOSITORY.Repository.Generic;

namespace DEMO.REPOSITORY.Repository.StudentRepo
{
    public class StudentRepository : GenericRepository, IStudentRepository
    {
        public StudentRepository(SchoolContext schoolContext) : base(schoolContext) { }
    }
}