﻿using DEMO.DAL.Models;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace DEMO.REPOSITORY.Repository.Generic
{
    public class GenericRepository : IGenericRepository
    {
        private readonly SchoolContext _context;
        private IUnitOfWork _unitOfWork;
        public GenericRepository(SchoolContext context)
        {
            _context = context;
        }

        public IEnumerable<TEntity> GetAll<TEntity>() where TEntity : class
        {
            IQueryable<TEntity> query = _context.Set<TEntity>();
            return query.ToList();
        }

        public IQueryable<TEntity> Query<TEntity>() where TEntity : class
        {
            IQueryable<TEntity> query = _context.Set<TEntity>();
            return query;
        }

        public TEntity GetByID<TEntity>(object id) where TEntity : class
        {
            return _context.Set<TEntity>().Find(id);
        }

        public TEntity Insert<TEntity>(TEntity entity) where TEntity : class
        {
            return _context.Set<TEntity>().Add(entity);
        }

        public void Delete<TEntity>(object id) where TEntity : class
        {
            TEntity entityToDelete = _context.Set<TEntity>().Find(id);
            Delete(entityToDelete);
        }

        public void Delete<TEntity>(TEntity entityToDelete) where TEntity : class
        {
            if (_context.Entry(entityToDelete).State == EntityState.Detached)
            {
                _context.Set<TEntity>().Attach(entityToDelete);
            }
            _context.Set<TEntity>().Remove(entityToDelete);
        }

        public void Update<TEntity>(TEntity entityToUpdate) where TEntity : class
        {
            _context.Set<TEntity>().Attach(entityToUpdate);
            _context.Entry(entityToUpdate).State = EntityState.Modified;
        }

        public IUnitOfWork UnitOfWork
        {
            get
            {
                if (_unitOfWork == null)
                {
                    _unitOfWork = new UnitOfWork(_context);
                }
                return _unitOfWork;
            }
        }
    }
}
