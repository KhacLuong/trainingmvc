﻿using System.Collections.Generic;
using System.Linq;

namespace DEMO.REPOSITORY.Repository.Generic
{
    public interface IGenericRepository
    {
        IEnumerable<TEntity> GetAll<TEntity>() where TEntity : class;
        TEntity GetByID<TEntity>(object id) where TEntity : class;
        IQueryable<TEntity> Query<TEntity>() where TEntity : class;
        TEntity Insert<TEntity>(TEntity entity) where TEntity : class;
        void Update<TEntity>(TEntity entity) where TEntity : class;
        void Delete<TEntity>(object id) where TEntity : class;
        IUnitOfWork UnitOfWork { get; }
    }
}
