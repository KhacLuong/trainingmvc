﻿using DEMO.DAL.Models;
using DEMO.REPOSITORY.Repository.Generic;

namespace DEMO.REPOSITORY.Repository.ClassRepo
{
    public interface IClassRepository : IGenericRepository
    {
    }
}
