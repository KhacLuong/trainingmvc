﻿using DEMO.DAL.Models;
using DEMO.REPOSITORY.Repository.Generic;

namespace DEMO.REPOSITORY.Repository.ClassRepo
{
    public class ClassRepository : GenericRepository, IClassRepository
    {
        public ClassRepository(SchoolContext context) : base(context)
        {
        }
    }
}