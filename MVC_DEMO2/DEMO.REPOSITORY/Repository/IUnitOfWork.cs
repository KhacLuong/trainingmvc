﻿using System;
using System.Data.Entity;

namespace DEMO.REPOSITORY.Repository
{
    public interface IUnitOfWork : IDisposable
    {
        int Complete();
        void Rollback();
    }
}
