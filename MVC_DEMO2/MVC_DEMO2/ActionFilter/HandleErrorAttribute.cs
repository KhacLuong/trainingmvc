﻿using MVC_DEMO2.Models.Log;
using System.Web.Mvc;
using System.Web.Routing;

namespace MVC_DEMO2.ActionFilter
{
    public class HandleErrorAttributeCustom : HandleErrorAttribute
    {
        public HandleErrorAttributeCustom() { }
        public override void OnException(ExceptionContext filterContext)
        {
            //if (filterContext.ExceptionHandled)
            //    return;
            ////write log exception
            //Log.Error(filterContext.Exception.Message);

            ////route to page show error 
            //filterContext.Result = new RedirectToRouteResult(
            //              new RouteValueDictionary
            //              {
            //                    { "controller", "Error" },
            //                    { "action", "Index" }
            //              });
            //// asp.net from showing yellow screen of death
            //filterContext.ExceptionHandled = true;

            ////erase any output already generated
            //filterContext.HttpContext.Response.Clear();
            //base.OnException(filterContext);
        }
    }
}