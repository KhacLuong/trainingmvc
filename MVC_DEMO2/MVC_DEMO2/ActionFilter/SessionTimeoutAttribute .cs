﻿using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;

namespace MVC_DEMO2.ActionFilter
{
    public class SessionTimeoutAttribute : ActionFilterAttribute
    {
        public SessionTimeoutAttribute() { }
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (filterContext == null || filterContext.HttpContext == null || filterContext.HttpContext.Request == null ||
                    (filterContext.ActionDescriptor.ControllerDescriptor.ControllerName == "AccountCustom"
                && filterContext.ActionDescriptor.ActionName == "Login"))
                return;
            HttpContext ctx = HttpContext.Current;
            if (HttpContext.Current.Session["UserInfo"] == null)
            {
                FormsAuthentication.SignOut();
                filterContext.Result = new RedirectToRouteResult(
                                  new RouteValueDictionary
                                  {
                                       { "action", "Login" },
                                       { "controller", "AccountCustom" }
                                  });
            }
            base.OnActionExecuting(filterContext);
        }

        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            base.OnActionExecuted(filterContext);
        }

        public override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            base.OnResultExecuting(filterContext);
        }

        public override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            base.OnResultExecuted(filterContext);
        }
    }
}