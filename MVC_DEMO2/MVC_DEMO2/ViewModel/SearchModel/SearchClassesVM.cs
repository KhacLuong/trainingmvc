﻿namespace MVC_DEMO2.ViewModel.SearchModel
{
    public class SearchClassesVM
    {
        public SearchClassesVM()
        {

        }
        public string ClassCode { get; set; }
        public string ClassName { get; set; }

        public int PageSize { get; set; }
        public int PageIndex { get; set; }
    }
}