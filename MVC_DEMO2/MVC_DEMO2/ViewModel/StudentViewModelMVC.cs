﻿using MVC_DEMO2.ViewModel.Model;
using MVC_DEMO2.ViewModel.SearchModel;
using PagedList;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace MVC_DEMO2.ViewModel
{
    public class StudentViewModelMVC
    {
        public StudentViewModelMVC()
        {
            SearchStudentVM = new SearchStudentVM();
            StudentViewModel = new StudentViewModel();
        }
        public StudentViewModel StudentViewModel { get; set; }

        public IEnumerable<SelectListItem> Classes { get; set; }

        public IPagedList<StudentViewModel> ListStudentVM { get; set; }

        public SearchStudentVM SearchStudentVM { get; set; }

    }
}