﻿using MVC_DEMO2.ViewModel.Model;
using MVC_DEMO2.ViewModel.SearchModel;
using Newtonsoft.Json;
using PagedList;
using System.ComponentModel.DataAnnotations;

namespace MVC_DEMO2.ViewModel
{
    public class ClassViewModelMVC
    {
        public ClassViewModelMVC()
        {
            SearchClassesVM = new SearchClassesVM();
        }
        public ClassViewModel ClassViewModel { get; set; }  

        public IPagedList<ClassViewModel> ListClassVM { get; set; }

        public SearchClassesVM SearchClassesVM { get; set; }
    }
}