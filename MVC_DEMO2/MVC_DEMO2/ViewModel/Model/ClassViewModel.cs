﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVC_DEMO2.ViewModel.Model
{
    public class ClassViewModel
    {   
        public ClassViewModel() { }
        public int ID { get; set; }
        [Required]
        [Display(Name = "Class Code")]
        public string ClassCode { get; set; }
        [Required]
        [Display(Name = "Class Name")]
        public string ClassName { get; set; }

    }
}