﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MVC_DEMO2.ViewModel.Model
{
    public class StudentViewModel
    {
        public StudentViewModel()
        {
            StudentCode = Guid.NewGuid().ToString();    
        }
        public int ID { get; set; }
        [Required]
        [Display(Name = "Student Code")]
        public string StudentCode { get; set; }
        [Required]
        [Display(Name = "Class Name")]
        public string ClassId { get; set; }
        [Required]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }
        [Required]
        [Display(Name = "First Name")]
        public string FirstMidName { get; set; }
        [Required]
        [DataType(DataType.Date)]
        public DateTime EnrollmentDate { get; set; }
        public IEnumerable<SelectListItem> Classes { get; set; }
    }
}