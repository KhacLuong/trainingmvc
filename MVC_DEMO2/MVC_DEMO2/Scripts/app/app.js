﻿$(document).ready(function () {
    $("#partialLoadData tbody tr th").on("change","input", function () {
        if (this.checked) {
            $('input:checkbox').not(this).prop('checked', true);
        }
        else {
            $('input:checkbox').not(this).prop('checked', false);
        }
    });
});