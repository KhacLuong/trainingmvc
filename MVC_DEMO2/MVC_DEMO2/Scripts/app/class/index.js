﻿$(document).ready(function () {

  
    //$("#searchform").on("submit", function (event) {
    //    debugger
    //    event.preventDefault();
    //    var modelSearch = $(this).serializeArray();
    //    jsonModelSearch = objectifyForm(modelSearch);
    //    jsonModelSearch.pageIndex = 1;
    //    jsonModelSearch.pageSize = 3;
    //    $.post(url, jsonModelSearch)
    //        .done(function (data) {
    //            $("#partialDataClass").html(data);
    //        });
    //});



    $("#partialLoadData").on("click", "li", function (event) {
        event.preventDefault();
        var jsonModelSearch;
        var url = "https://localhost:44347/ClassesArea/Classes/SearchCondition";
        var modelSearch = $("#searchform").serializeArray();
        jsonModelSearch = objectifyForm(modelSearch);
        var pageIndex = $(this)[0].children[0].text
        jsonModelSearch.pageIndex = parseInt(pageIndex);
        jsonModelSearch.pageSize = 3;
        console.log(pageIndex)
        $.post(url, jsonModelSearch)
            .done(function (data) {
                $("#partialLoadData").html(data);
            });
    })

    function objectifyForm(formArray) {
        //serialize data function
        var returnArray = {};
        for (var i = 0; i < formArray.length; i++) {
            returnArray[formArray[i]['name']] = formArray[i]['value'];
        }
        return returnArray;
    }

});