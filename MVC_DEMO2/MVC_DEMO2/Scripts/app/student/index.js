﻿$(document).ready(function () {
    var jsonModelSearch;

    $("#partialLoadData").on("click", "li", function (event) {
        event.preventDefault();
        var url = "https://localhost:44347/StudentsArea/Students/SearchCondition";
        var modelSearch = $("#searchform").serializeArray();
        jsonModelSearch = objectifyForm(modelSearch);
        var pageIndex = $(this)[0].children[0].text
        jsonModelSearch.pageIndex = parseInt(pageIndex);
        jsonModelSearch.pageSize = 3;
        console.log(pageIndex)
        $.post(url, jsonModelSearch)
            .done(function (data) {
                $("#partialLoadData").html(data);
            });
    })

    $("#partialLoadData table tbody ").on("click", "tr .btn.btn-danger", function (event) {
        debugger
        var row = $(this).parent().parent();
        var id = row.find("td:first input")[0].value;
        var urlDelete = "https://localhost:44362/api/Classes/DeleteClass?id=" + id;
        var token = getCookie("tokenAPI");
        $.ajax({
            headers: {
                'Authorization': "Bearer " + token
            },
            url: urlDelete,
            method: "GET",
            dataType: 'json',
            success: function (msg) {
                debugger
                row.remove();
            },
            error: function (jqXHR, textStatus, err) {
                debugger
            }
        });
    });

    function getCookie(name) {
        const value = `; ${document.cookie}`;
        const parts = value.split(`; ${name}=`);
        if (parts.length === 2) return parts.pop().split(';').shift();
    }


    function objectifyForm(formArray) {
        //serialize data function
        var returnArray = {};
        for (var i = 0; i < formArray.length; i++) {
            returnArray[formArray[i]['name']] = formArray[i]['value'];
        }
        return returnArray;
    }

    function getCookie(name) {
        const value = `; ${document.cookie}`;
        const parts = value.split(`; ${name}=`);
        if (parts.length === 2) return parts.pop().split(';').shift();
    }
});