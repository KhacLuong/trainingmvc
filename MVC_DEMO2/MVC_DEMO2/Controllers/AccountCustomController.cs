﻿using DEMO.DAL.Models;
using MVC_DEMO2.Helpers;
using MVC_DEMO2.Models;
using MVC_DEMO2.Models.Log;
using MVC_DEMO2.ViewModel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace MVC_DEMO2.Controllers
{
    public class AccountCustomController : Controller
    {
        private WebClientHelper _webClient;
        public AccountCustomController()
        {
            _webClient = new WebClientHelper(); 
        }
        [HttpGet]
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            UserMasterVM model = new UserMasterVM
            {
                UserEmail = "khacluong@gmail.com",
                Password = "P@ssword00"
            };
            return View(model);
        }

        //[HttpPost]
        //[AllowAnonymous]
        //public ActionResult Login(UserMasterVM model, string returnUrl)
        //{
        //    try
        //    {
        //        if (!ModelState.IsValid)
        //        {
        //            return View(model);
        //        }

        //        var result = VerifyUserNamePassword(model);
        //        if (result)
        //        {
        //            HttpContext.Session["UserInfo"] = model;
        //            //HttpContext.Session.Timeout = 30;
        //            FormsAuthentication.SetAuthCookie(model.UserEmail, true);
        //            return RedirectToAction("Index", "Students", new { area = "Students" });
        //        }
        //        else
        //        {
        //            ModelState.AddModelError("", "Invalid username or password");
        //        }
        //        ViewBag.ReturnUrl = returnUrl;
        //        return View(model);
        //    }
        //    catch (ExceptionCustom ex)
        //    {
        //        var errorMessage = string.Format("Error Code {0}, Message error: {1}", ex.ErrorCode, ex.ErrorMessage);
        //        Log.Error(errorMessage, ex);
        //        ModelState.AddModelError("", ex.ErrorMessage);
        //    }
        //    return View(model);

        //}

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Login(UserMasterVM model, string returnUrl)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return View(model);
                }

                var respone = _webClient.Post(UrlAPI.LoginAPI, model);
                if(respone != null)
                {
                    HttpContext.Session["UserInfo"] = model;
                    //HttpContext.Session.Timeout = 30;
                    var result = JsonConvert.DeserializeObject<Dictionary<string, string>>(respone);
                    var token = result["data"];
                    HttpCookie ck = new HttpCookie("tokenAPI", token);
                    Response.Cookies.Add(ck);
                    FormsAuthentication.SetAuthCookie(model.UserEmail, true);
                    return RedirectToAction("Index", "Students", new { area = "Students" });
                }
                return View(model);
            }
            catch (ExceptionCustom ex)
            {
                var errorMessage = string.Format("Error Code {0}, Message error: {1}", ex.ErrorCode, ex.ErrorMessage);
                Log.Error(errorMessage, ex);
                ModelState.AddModelError("", ex.ErrorMessage);
            }
            return View(model);

        }


        [HttpGet]
        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home", new { area = "" });
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        private bool VerifyUserNamePassword(UserMasterVM userMasterVM)
        {
            using (SchoolContext context = new SchoolContext())
            {
                bool IsValidUser = context.UserMaster.Any(user => user.UserEmail.ToLower() ==
                     userMasterVM.UserEmail.ToLower() && user.Password == userMasterVM.Password);
                if (!IsValidUser)
                {
                    throw new ExceptionCustom(1000, string.Format("username [{0}] or password [{1}] is not correct", userMasterVM.UserEmail, userMasterVM.Password));
                }
                return IsValidUser;
            }
        }
    }
}