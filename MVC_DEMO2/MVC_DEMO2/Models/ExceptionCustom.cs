﻿using System;

namespace MVC_DEMO2.Models
{
    public class ExceptionCustom : Exception
    {
        public int ErrorCode { get; set; }
        public string ErrorMessage { get; set; }

        public ExceptionCustom(int errorCode, string errorMessage) : base(errorMessage)
        {
            ErrorCode = errorCode;
            ErrorMessage = errorMessage;
        }
    }
}