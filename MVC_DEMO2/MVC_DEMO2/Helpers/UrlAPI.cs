﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVC_DEMO2.Helpers
{
    public static class UrlAPI
    {
        private static string baseAddress = @"https://localhost:44362/";
        //class
        public static string GetClassById = baseAddress + "api/Classes/GetClassById";
        public static string GetClasses = baseAddress + "api/Classes/GetClasses";
        public static string GetClassesByCondition = baseAddress + "api/Classes/GetClassesByCondition";
        public static string UpdateClass = baseAddress + "api/Classes/UpdateClass";
        public static string CreateClass = baseAddress + "api/Classes/CreateClass";
        public static string DeleteClass = baseAddress + "api/Classes/DeleteClass";
        public static string DeleteClassSelected = baseAddress + "api/Classes/DeleteSelected";
        //student
        public static string GetStudentsById = baseAddress + "api/Students/GetStudentsById";
        public static string GetStudents = baseAddress + "api/Students/GetStudents";
        public static string GetStudentByCondition = baseAddress + "api/Students/GetStudentByCondition";
        public static string UpdateStudent = baseAddress + "api/Students/UpdateStudent";
        public static string CreateStudent = baseAddress + "api/Students/CreateStudent";
        public static string DeleteStudent = baseAddress + "api/Students/DeleteStudent";
        public static string DeleteStudentSelected = baseAddress + "api/Students/DeleteSelected";
        //login
        public static string LoginAPI = baseAddress + "api/Token/GetToken";
    }
}