﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace MVC_DEMO2.Helpers
{
    public class WebClientHelper
    {
        private string _token;
        public WebClientHelper(string token)
        {
            _token = token;
        }
        public WebClientHelper()
        {
        }
        public enum Method
        {
            GET,
            POST,
        }

        public string Get(string url)
        {
            using (var webClient = new WebClient())
            {
                webClient.Headers.Add("content-type", "application/json");
                webClient.Headers.Add("Authorization", "Bearer "+ _token);
                string response = webClient.DownloadString(url);
                return response;
            }
        }

        public string Get(string url, Dictionary<string, string> dic)
        {
            using (var webClient = new WebClient())
            {
                webClient.Headers.Add("content-type", "application/json");
                webClient.Headers.Add("Authorization", "Bearer " + _token);
                foreach (var item in dic)
                {
                    webClient.QueryString.Add(item.Key, item.Value);
                }
                webClient.Headers.Add("content-type", "application/json");
                string response = webClient.DownloadString(url);
                return response;
            }
        }

        public string Post(string url, object obj)
        {
            using (var webClient = new WebClient())
            {
                webClient.Headers.Add("content-type", "application/json");
                webClient.Headers.Add("Authorization", "Bearer " + _token);
                string response = Encoding.ASCII.GetString(webClient.UploadData(url, "POST", Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(obj))));
                return response;
            }
        }
    }
}