﻿using MVC_DEMO2.Helpers;
using MVC_DEMO2.ViewModel;
using MVC_DEMO2.ViewModel.Model;
using MVC_DEMO2.ViewModel.SearchModel;
using Newtonsoft.Json;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace MVC_DEMO2.Areas.Students.Controllers
{
    [Authorize]
    public class StudentsController : Controller
    {
        private WebClientHelper ClientHelper
        {
            get
            {
                var token = Request.Cookies.Get("tokenAPI").Value;
                return new WebClientHelper(token);
            }
        }
        public StudentsController()
        {

        }

        public ActionResult Index()
        {
            var messError = TempData["MessageError"] as string;
            if (messError != null)
            {
                ModelState.AddModelError(string.Empty, messError);
            }
            int pageSize = 3;
            int pageIndex = 1;
            var response = ClientHelper.Get(UrlAPI.GetStudents);
            var modelView = new StudentViewModelMVC();
            if (response != null)
            {
                var result = JsonConvert.DeserializeObject<IEnumerable<StudentViewModel>>(response);
                modelView.ListStudentVM = result.ToPagedList(pageIndex, pageSize);
                modelView.StudentViewModel.Classes = _selectListItem;
            }
            return View(modelView);
        }

        [HttpPost]
        public ActionResult Index(List<int> ids, string action)
        {
            switch (action)
            {
                case "DeleteSelected":
                    return DeleteSeleted(ids);
                default:
                    return RedirectToAction("Index");
            }
        }

        [HttpPost]
        public ActionResult SearchCondition(SearchStudentVM searchModel)
        {
            searchModel.PageIndex = searchModel.PageIndex == 0 ? 1 : searchModel.PageIndex;
            searchModel.PageSize = 3;
            var response = ClientHelper.Post(UrlAPI.GetStudentByCondition, searchModel);
            var modelView = new StudentViewModelMVC();
            if (response != null)
            {
                var result = JsonConvert.DeserializeObject<IEnumerable<StudentViewModel>>(response);
                modelView.ListStudentVM = result.ToPagedList(searchModel.PageIndex, searchModel.PageSize);
                modelView.SearchStudentVM = searchModel;
            }
            return PartialView("_PartialViewSearchStudent", modelView);
        }

        // GET: Students/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var param = new Dictionary<string, string>() { { "id", id.ToString() } };
            var response = ClientHelper.Get(UrlAPI.GetStudentsById, param);
            var studentVM = new StudentViewModel();
            if (response != null)
            {
                studentVM = JsonConvert.DeserializeObject<StudentViewModel>(response);
                studentVM.Classes = _selectListItem;
                if (studentVM == null)
                {
                    return HttpNotFound();
                }
            }
            return View(studentVM);
        }

        [HttpGet]
        public ActionResult Create()
        {
            var studentVM = new StudentViewModel
            {
                StudentCode = Guid.NewGuid().ToString(),
                Classes = _selectListItem,
            };
            return PartialView(studentVM);
        }

        [HttpPost]
        public ActionResult Create(StudentViewModel studentvm)
        {
            studentvm.Classes = _selectListItem;
            if (ModelState.IsValid)
            {
                var response = ClientHelper.Post(UrlAPI.CreateStudent, studentvm);
                if (response != null)
                {
                    var result = JsonConvert.DeserializeObject<StudentViewModel>(response);
                }
                ViewBag.MessageSuccess = String.Format("Create student {0} {1} success", studentvm.LastName, studentvm.FirstMidName);
                return PartialView("_PartialStudent", new StudentViewModel
                {
                    StudentCode = Guid.NewGuid().ToString(),
                    Classes = _selectListItem
                });
            }
            return PartialView("_PartialStudent", studentvm);
        }

        [HttpGet]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //get student
            var param = new Dictionary<string, string>() { { "id", id.ToString() } };
            var response = ClientHelper.Get(UrlAPI.GetStudentsById, param);
            var studentVM = new StudentViewModel();
            if (response != null)
            {
                studentVM = JsonConvert.DeserializeObject<StudentViewModel>(response);
                studentVM.Classes = _selectListItem;
                if (studentVM == null)
                {
                    return HttpNotFound();
                }
            }
            return PartialView("_PartialStudent", studentVM);
        }

        [HttpPost]
        public ActionResult Edit(StudentViewModel studentVM)
        {
            studentVM.Classes = _selectListItem;
            if (ModelState.IsValid)
            {
                var response = ClientHelper.Post(UrlAPI.UpdateStudent, studentVM);
                if (response != null)
                {
                    ViewBag.MessageSuccess = "Update Sucess";

                    return PartialView("_PartialStudent", studentVM);
                }
            }
            return PartialView("_PartialStudent", studentVM);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var param = new Dictionary<string, string>() { { "id", id.ToString() } };
            var response = ClientHelper.Get(UrlAPI.GetStudentsById, param);
            var studentVM = new StudentViewModel();
            if (response != null)
            {
                studentVM = JsonConvert.DeserializeObject<StudentViewModel>(response);
                if (studentVM == null)
                {
                    return HttpNotFound();
                }
            }
            return View(studentVM);
        }

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            var param = new Dictionary<string, string>() { { "id", id.ToString() } };
            var response = ClientHelper.Get(UrlAPI.DeleteStudent, param);
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult DeleteSeleted(List<int> ids)
        {
            var response = ClientHelper.Post(UrlAPI.DeleteStudentSelected, ids);
            return RedirectToAction("Index");
        }

        private IEnumerable<SelectListItem> _selectListItem
        {
            get
            {
                var response = ClientHelper.Get(UrlAPI.GetClasses);
                if (response != null)
                {
                    var result = JsonConvert.DeserializeObject<List<ClassViewModel>>(response);
                    return result.Select(c => new SelectListItem
                    {
                        Value = c.ID.ToString(),
                        Text = c.ClassName
                    });
                }
                return Enumerable.Empty<SelectListItem>();
            }
        }
    }
}