﻿using System.Web.Mvc;

namespace MVC_DEMO2.Areas.Students
{
    public class StudentsAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Students";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Students_default",
                "StudentsArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}