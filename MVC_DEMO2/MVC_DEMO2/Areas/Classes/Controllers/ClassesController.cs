﻿using MVC_DEMO2.Helpers;
using MVC_DEMO2.Models.Log;
using MVC_DEMO2.ViewModel;
using MVC_DEMO2.ViewModel.Model;
using MVC_DEMO2.ViewModel.SearchModel;
using Newtonsoft.Json;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace MVC_DEMO2.Areas.Classes.Controllers
{
    [Authorize]
    public class ClassesController : Controller
    {
        private WebClientHelper ClientHelper
        {
            get
            {
                var token = Request.Cookies.Get("tokenAPI").Value;
                return new WebClientHelper(token);
            }
        }
        public ClassesController()
        {
          
        }
        // GET: Class/Classes
        public ActionResult Index()
        {
            var messError = TempData["MessageError"] as string;
            if (messError != null)
            {
                ModelState.AddModelError(string.Empty, messError);
            }
            int pageSize = 3;
            int pageIndex = 1;
            var response = ClientHelper.Get(UrlAPI.GetClasses);

            var modelView = new ClassViewModelMVC();
            if (response != null)
            {
                var lstResult = JsonConvert.DeserializeObject<List<ClassViewModel>>(response);
                modelView.ListClassVM = lstResult.ToPagedList(pageIndex, pageSize);
            }
            return View(modelView);
        }

        [HttpPost]
        public ActionResult Index(List<int> ids, string action)
        {
            switch (action)
            {
                case "DeleteSelected":
                    return DeleteSeleted(ids);
                default:
                    return RedirectToAction("Index");
            }
        }

        [HttpPost]
        public ActionResult SearchCondition(SearchClassesVM searchModel)
        {
            var response = ClientHelper.Post(UrlAPI.GetClassesByCondition, searchModel);
            var modelView = new ClassViewModelMVC();
            if (response != null)
            {
                var lstResult = JsonConvert.DeserializeObject<List<ClassViewModel>>(response);
                modelView.ListClassVM = lstResult.ToPagedList(searchModel.PageIndex, searchModel.PageSize);
            }
            return PartialView("_PartialViewSearchClass", modelView);
        }

        // GET: Class/Classes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var currentClassVM = new ClassViewModel();
            var param = new Dictionary<string, string>() { { "id", id.ToString() } };
            var respone = ClientHelper.Get(UrlAPI.GetClassById, param);
            if (respone != null)
            {
                currentClassVM = JsonConvert.DeserializeObject<ClassViewModel>(respone);
            }
            return View(currentClassVM);
        }

        // GET: Class/Classes/Create
        public ActionResult Create()
        {
            var classVM = new ClassViewModel();
            classVM.ClassCode = Guid.NewGuid().ToString();

            return View(classVM);
        }

        // POST: Class/Classes/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ClassViewModel classVM)
        {
            if (ModelState.IsValid)
            {
                var response = ClientHelper.Post(UrlAPI.CreateClass, classVM);
                if (response != null)
                {
                    var result = JsonConvert.DeserializeObject<ClassViewModel>(response);
                }
                ViewBag.MessageSuccess = "Create success Class: " + classVM.ClassName;
            }
            return View(classVM);
        }

        [HttpGet]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var currentClassVM = new ClassViewModel();
            var param = new Dictionary<string, string>() { { "id", id.ToString() } };
            var respone = ClientHelper.Get(UrlAPI.GetClassById, param);
            if (respone != null)
            {
                currentClassVM = JsonConvert.DeserializeObject<ClassViewModel>(respone);
            }
            if (currentClassVM == null)
            {
                return HttpNotFound();
            }

            return View(currentClassVM);
        }

        // POST: Class/Classes/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ClassViewModel classVM)
        {
            if (ModelState.IsValid)
            {
                var response = ClientHelper.Post(UrlAPI.UpdateClass, classVM);
                if (response != null)
                {
                    var result = JsonConvert.DeserializeObject<bool>(response);
                }
                return RedirectToAction("Index");
            }
            return View(classVM);
        }

        // GET: Class/Classes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var currentClassVM = new ClassViewModel();
            var param = new Dictionary<string, string>() { { "id", id.ToString() } };
            var respone = ClientHelper.Get(UrlAPI.GetClassById, param);
            if (respone != null)
            {
                 currentClassVM = JsonConvert.DeserializeObject<ClassViewModel>(respone);
            }
            if (currentClassVM == null)
            {
                return HttpNotFound();
            }
            return View(currentClassVM);
        }

        // POST: Class/Classes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                var param = new Dictionary<string, string>() { { "id", id.ToString() } };
                var response = ClientHelper.Get(UrlAPI.DeleteClass, param);
                var jsonResponse = JsonConvert.DeserializeObject<bool>(response);
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                ModelState.AddModelError("", ex.Message);
            }
            return RedirectToAction("Index");
        }


        [HttpPost, ActionName("DeleteSeleted")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteSeleted(List<int> ids)
        {
            try
            {
                var response = ClientHelper.Post(UrlAPI.DeleteClassSelected, ids);
                var jsonResponse = JsonConvert.DeserializeObject<bool>(response);
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                ModelState.AddModelError("", ex.Message);
                TempData["MessageError"] = ex.Message;
            }
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult ListStudent(int? classId)
        {
            var respone = ClientHelper.Get(UrlAPI.GetStudents);
            var listStudentVM = new List<StudentViewModel>();
            if (respone != null)
            {
                listStudentVM = JsonConvert.DeserializeObject<List<StudentViewModel>>(respone)
                    .Where(x => x.ClassId == classId.ToString()).ToList();
            }
            return View(listStudentVM);
        }
    }
}
