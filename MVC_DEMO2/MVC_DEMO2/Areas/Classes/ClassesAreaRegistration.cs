﻿using System.Web.Mvc;

namespace MVC_DEMO2.Areas.Classes
{
    public class ClassesAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Classes";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Classes_default",
                "ClassesArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}