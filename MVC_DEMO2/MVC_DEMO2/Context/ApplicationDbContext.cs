﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace MVC_DEMO2.DAL
{
    public class ApplicatonContext : DbContext
    {
        public ApplicatonContext() : base("DefaultConnection")
        {
        }
        
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}