﻿using MVC_DEMO2.ActionFilter;
using System.Web.Mvc;

namespace MVC_DEMO2
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttributeCustom());
            filters.Add(new SessionTimeoutAttribute());
        }
    }
}
